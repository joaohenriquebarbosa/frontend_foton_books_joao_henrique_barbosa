# FOTON Books - Documentação

Este projeto foi desenvolvido como avaliação técnica para a FOTON. 
O link para o app é [https://evening-anchorage-73591.herokuapp.com/](https://evening-anchorage-73591.herokuapp.com/) 

Repare que a primeira vez que a página carregar é comum que demore um pouco, isso de nada tem a ver com a performance do app mas sim com o servidor estar em um plano gratuito no qual ele hiberna depois de um tempo de inatividade.

## Introdução
O desenvolvimento desse sistema foi muito divertido, sinto que coloquei em prática boa parte do que eu sei de React, Node e MongoDB, optei por React web pois é onde eu me sinto mais confortável de trabalhar, inclusive acredito que aprendi coisas novas no decorrer das atividades. 

## Dificuldades
A maior dificuldade que eu encontrei foi o tempo, eu queria fazer muito mais coisas, porém estou satisfeito com o resultado. A falta de tempo me levou a fazer algumas decisões criativas, por exemplo a imagem de capa do livro que ao invés de fazer um upload eu decidi deixar apenas que o usuário cadastre o link de uma imagem na internet.
Apesar de eu julgar 7 dias mais do que necessários, eu gostaria de ter aproveitando-os em sua totalidade, em meio a problemas pessoais, e horas extras que precisei cumprir no meu serviço atual, me restou pouco tempo para me dedicar a este projeto.

## Estilo e design
Todo o visual foi baseado no [design do Figma](https://www.figma.com/file/KFElqzD983WNyvMY1SaF0c/frontend-book-app?node-id=0:1), levei a risca a instrução que dizia para criar o app uma cópia "pixel perfect", muitas vezes lançando mão de técnicas que eu não estou acostumado para poder alcançar o design. Criei diversos componentes, e inclusive um hook (useDeclarativeStyles) para ajudar no desenvolvimento do design.

## Funcionalidades além do pedido

- Edição de livros;
- Exclusão de livros;

## TODO
Algumas funcionalidades eu gostaria de ter feito, porém em razão do tempo não o fiz. São elas:

- Autenticação;
- Perfil de usuário;
- Paginação;
- Upload de imagens das capas;

## API
> Endpoint: https://boiling-everglades-96721.herokuapp.com/api/

| Método | URN | Ação |
|--|--|--|
| GET | /books | Retornar todos os livros cadastrados. |
| POST | /books | Cadastrar um novo livro. |
| GET | /books/:id| Retornar o livro de id solicitado. |
| PUT | /books/:id | Alterar o livro de id solicitado. |
| DELETE | /books/:id| Excluir o livro de id solicitado. |


## Técnologias utilizadas

- **Back-end:** Node e Express
- **Banco de dados:** MongoDB
- **Front-end:** React e create-react-app
- **Hospedagem gratuita escolhida:** Heroku
- **Editor de texto:** Visual Studio Code
- **Package manager:** Yarn

## Principais dependências
Back-end:

- body-parser
- cors
- express
- dotenv
- mongoose
- express-mongo-sanitize
- winston
- http-status
- xss-clean

Front-end:

 - react
 - react-router-dom
 - styled-components
 - styled-reset
 - axios
 - lodash
 - react-loader-spinner
