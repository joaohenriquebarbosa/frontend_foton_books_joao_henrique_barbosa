import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import theme from "./styles/theme";
import GlobalStyle from "./styles/global.style";
import { Reset } from "styled-reset";
import Home from "./screens/home";
import BottomNavigation from "./components/BottomNavigation";
import { AppWrapper } from "./styles/layout.style";
import BookDetail from "./screens/bookDetail";
import BookForm from "./screens/bookForm";

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <GlobalStyle />
        <Reset />
        <AppWrapper>
          <Switch>
            <Route path="/book/new">
              <BookForm />
              <BottomNavigation />
            </Route>
            <Route path="/book/edit/:id">
              <BookForm />
              <BottomNavigation />
            </Route>
            <Route path="/book/:id">
              <BookDetail />
            </Route>
            <Route path="/">
              <Home />
              <BottomNavigation />
            </Route>
          </Switch>
        </AppWrapper>
      </Router>
    </ThemeProvider>
  );
};

export default App;
