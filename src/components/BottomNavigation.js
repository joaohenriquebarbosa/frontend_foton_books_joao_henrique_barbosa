import React from "react";
import { BottomNavigationWrapper, BottomNavItem } from "../styles/layout.style";
import Box from "./Box";
import Typography from "./Typography";

const BottomNavigation = () => {
  return (
    <BottomNavigationWrapper>
      <Box width="375px" display="flex">
        <BottomNavItem exact to="/">
          <img alt="home" src="/assets/icons/home.svg" />
          <Typography fontSize="12px" marginTop="8px">
            Home
          </Typography>
        </BottomNavItem>
        <BottomNavItem exact to="/book/new">
          <img alt="add book" src="/assets/icons/+.svg" />
          <Typography fontSize="12px" marginTop="8px">
            Add book
          </Typography>
        </BottomNavItem>
        <BottomNavItem exact to="/profile">
          <img height="20" alt="profile" src="/assets/icons/user.svg" />
          <Typography fontSize="12px" marginTop="8px">
            Profile
          </Typography>
        </BottomNavItem>
      </Box>
    </BottomNavigationWrapper>
  );
};

export default BottomNavigation;
