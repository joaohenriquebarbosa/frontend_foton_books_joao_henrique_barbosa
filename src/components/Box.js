import useDeclarativeStyles from "../hooks/useDeclarativeStyles";

const Box = ({ children, ...rest }) => {
  const styles = useDeclarativeStyles(rest);

  return <div style={styles}>{children}</div>;
};

export default Box;
