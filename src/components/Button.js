import React from "react";
import useDeclarativeStyles from "../hooks/useDeclarativeStyles";
import { ButtonWrapper } from "../styles/global.style";
import Typography from "./Typography";

const Button = ({ children, background = "primary", ...props }) => {
  const styles = useDeclarativeStyles({ ...props, background: background });

  return (
    <ButtonWrapper style={styles} {...props}>
      <Typography fontSize="24px" color="#ffffff">
        {children}
      </Typography>
    </ButtonWrapper>
  );
};

export default Button;
