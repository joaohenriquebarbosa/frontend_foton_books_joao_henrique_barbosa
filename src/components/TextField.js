import React, {
  forwardRef,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import useDeclarativeStyles from "../hooks/useDeclarativeStyles";
import { TextFieldWrapper } from "../styles/form.style";
import Typography from "./Typography";

const EmptyComponent = ({ children }) => <>{children}</>;
const TextFieldLabel = ({ children, label }) => (
  <label>
    <Typography
      font="text"
      fontSize="16px"
      fontWeight="600"
      marginBottom="10px"
    >
      {label}
    </Typography>
    {children}
  </label>
);

const TextField = forwardRef(
  (
    {
      search,
      label,
      multiline,
      onChange,
      required,
      generalTouched,
      defaultValue,
      ...props
    },
    ref
  ) => {
    const styles = useDeclarativeStyles(props);
    const [value, setvalue] = useState(defaultValue || "");
    const [touched, setTouched] = useState(false);
    const [error, setError] = useState(null);
    const internalRef = useRef();

    let LabelComponent = EmptyComponent;
    if (label) LabelComponent = TextFieldLabel;

    const handleChange = (e) => {
      const newVal = e.target.value;
      setvalue(newVal);
      onChange?.(e);
      !touched && setTouched(true);
      if (required && newVal === "") {
        setError("This field is required");
      } else {
        setError(null);
      }
    };

    useImperativeHandle(ref, () => ({
      getValue: () => {
        return value;
      },
      isValid: () => {
        if (required && value === "") {
          setError("This field is required");
          internalRef.current?.focus?.();
          return false;
        }
        setError(null);
        internalRef.current?.focus?.();
        return true;
      },
    }));

    return (
      <div style={styles}>
        <LabelComponent label={label}>
          <TextFieldWrapper $search={search} $multiline={multiline}>
            {search && (
              <img alt="search icon" src="/assets/icons/search-icon.svg" />
            )}
            {multiline ? (
              <textarea
                ref={internalRef}
                value={value}
                onChange={handleChange}
                {...props}
              />
            ) : (
              <input
                ref={internalRef}
                value={value}
                onChange={handleChange}
                {...props}
              />
            )}
          </TextFieldWrapper>
          {(generalTouched || touched) && error && (
            <Typography mt={1} fontSize="12px" color="red">
              {error}
            </Typography>
          )}
        </LabelComponent>
      </div>
    );
  }
);

export default TextField;
