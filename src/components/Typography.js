import React from "react";
import useDeclarativeStyles from "../hooks/useDeclarativeStyles";
import { TypographyWrapper } from "../styles/layout.style";

const Typography = ({ font, children, elipsis, ...props }) => {
  const styles = useDeclarativeStyles(props);

  return (
    <TypographyWrapper className={font} elipsis={elipsis} style={styles}>
      {children}
    </TypographyWrapper>
  );
};

export default Typography;
