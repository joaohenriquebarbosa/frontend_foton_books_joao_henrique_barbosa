import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://boiling-everglades-96721.herokuapp.com/api",
});

apiClient.interceptors.response.use(
  (response) => response.data,
  async (error) => {
    return Promise.reject(error.response.data);
  }
);

const { get, post, put, delete: destroy } = apiClient;
export { get, post, put, destroy };
