import styled from "styled-components";

export const BookHeader = styled.div`
  background-color: #fff6e5;
  height: 282px;
  width: 100%;
  border-radius: 0 0 100px;
`;

export const CoverWrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  height: 295px;
  margin-top: -227px;
  background: url("/assets/images/book-bg.png");
  background-repeat: no-repeat;
  background-position-x: center;
  padding-top: 30px;
`;

export const HeaderAdornment = styled.img`
  position: absolute;
  right: 0;
  top: 0;
`;

export const BackButton = styled.button`
  background: none;
  border: none;
  position: absolute;
  left: 32px;
  top: 55px;
`;

export const BookFooter = styled.div`
  font-size: 14px;
  font-weight: bold;
  color: ${({ theme }) => theme.palette.bookActions};
  width: 100%;
  display: flex;
  justify-content: center;
  position: fixed;
  bottom: 30px;

  & > div {
    display: flex;
    height: 56px;
    width: 295px;
    justify-content: space-between;
    align-items: center;
    background: #ffffff;
    box-shadow: 3px 3px 23px rgba(107, 103, 70, 0.125901);
    border-radius: 2px;
    padding: 0 20px 0 20px;

    div,
    div a {
      display: flex;
      align-items: center;
      text-decoration: none;
      color: inherit;

      img {
        margin-right: 10px;
      }
    }
  }
`;
