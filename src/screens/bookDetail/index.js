import React, { useEffect, useState } from "react";
import Loader from "react-loader-spinner";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import Box from "../../components/Box";
import Typography from "../../components/Typography";
import booksService from "../../services/books.service";
import { Cover } from "../../styles/global.style";
import { Container } from "../../styles/layout.style";
import theme from "../../styles/theme";
import {
  BackButton,
  BookHeader,
  CoverWrapper,
  HeaderAdornment,
  BookFooter,
} from "./book.style";

const BookDetail = () => {
  const { id } = useParams();
  const [book, setBook] = useState(null);

  useEffect(() => {
    booksService.getBook(id).then((resp) => setBook(resp));
  }, [id]);

  if (!book) {
    return (
      <Box display="flex" justifyContent="center" mt="36px" width="100%">
        <Loader
          type="TailSpin"
          color={theme.palette.primary}
          height={80}
          width={80}
        />
      </Box>
    );
  }

  return (
    <div>
      <BookHeader>
        <BackButton>
          <Link to="/">
            <img alt="back icon" src="/assets/icons/back.svg" />
          </Link>
        </BackButton>
        <HeaderAdornment alt="header adornment" src="/assets/images/oval.png" />
      </BookHeader>
      <CoverWrapper>
        <Cover width="153" height="229" alt="book cover" src={book.cover} />
      </CoverWrapper>
      <Box mt="-30px">
        <Container>
          <Typography fontSize="24px" color="textAccent">
            {book.name}
          </Typography>
          <Typography
            marginTop="7px"
            fontSize="16px"
            color="primary"
            fontWeight="600"
          >
            {book.author}
          </Typography>
          <Typography marginTop="20px" fontSize="14px" font="text" color="text">
            {book.description}
          </Typography>
        </Container>
      </Box>
      <Box mt={6} />
      <BookFooter>
        <div>
          <div>
            <img alt="book icon" src="/assets/icons/book-open.svg" />
            Read
          </div>
          <img alt="line icon" height="16" src="/assets/icons/line.svg" />
          <div>
            <img alt="headphones icon" src="/assets/icons/headphones.svg" />
            Listen
          </div>
          <img alt="line icon" height="16" src="/assets/icons/line.svg" />
          <div>
            <Link to={`/book/edit/${book._id}`}>
              <img alt="pencil icon" src="/assets/icons/pencil.svg" />
              Edit
            </Link>
          </div>
        </div>
      </BookFooter>
    </div>
  );
};

export default BookDetail;
