import React, { useEffect, useRef, useState } from "react";
import Loader from "react-loader-spinner";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import Box from "../../components/Box";
import Button from "../../components/Button";
import TextField from "../../components/TextField";
import Typography from "../../components/Typography";
import booksService from "../../services/books.service";
import { Container } from "../../styles/layout.style";
import theme from "../../styles/theme";

const BookForm = () => {
  const { id } = useParams();
  const edit = Boolean(id);
  const [formTouched, setFormTouched] = useState(false);
  const [book, setBook] = useState(null);
  const [loading, setLoading] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const [success, setSuccess] = useState(false);
  const nameRef = useRef();
  const authorRef = useRef();
  const descriptionRef = useRef();
  const coverRef = useRef();

  useEffect(() => {
    if (edit) {
      booksService.getBook(id).then((resp) => setBook(resp));
    }
  }, [edit, id]);

  const onSubmit = () => {
    setFormTouched(true);
    if (
      nameRef.current.isValid() &&
      authorRef.current.isValid() &&
      descriptionRef.current.isValid() &&
      coverRef.current.isValid()
    ) {
      const formData = {
        name: nameRef.current.getValue(),
        author: authorRef.current.getValue(),
        description: descriptionRef.current.getValue(),
        cover: coverRef.current.getValue(),
      };
      if (edit) {
        setLoading(true);
        booksService
          .updateBook(book._id, formData)
          .then(() => {
            setSubmitted(true);
            setSuccess(true);
          })
          .catch(() => {
            setSubmitted(true);
            setSuccess(false);
          })
          .finally(() => setLoading(false));
      } else {
        setLoading(true);
        booksService
          .createBook(formData)
          .then(() => {
            setSubmitted(true);
            setSuccess(true);
          })
          .catch(() => {
            setSubmitted(true);
            setSuccess(false);
          })
          .finally(() => setLoading(false));
      }
    }
  };

  const onDelete = () => {
    setLoading(true);
    booksService
      .deleteBook(book._id)
      .then(() => {
        setSubmitted(true);
        setSuccess(true);
      })
      .catch(() => {
        setSubmitted(true);
        setSuccess(false);
      })
      .finally(() => setLoading(false));
  };

  if ((edit && !book) || loading) {
    return (
      <Box display="flex" justifyContent="center" mt="36px" width="100%">
        <Loader
          type="TailSpin"
          color={theme.palette.primary}
          height={80}
          width={80}
        />
      </Box>
    );
  }

  if (submitted) {
    return (
      <Box
        display="flex"
        height="100vh"
        width="100%"
        justifyContent="center"
        alignItems="center"
        flexDirection="column"
      >
        <Typography mb={3} fontSize="24px" color="primary" fontWeight="600">
          {success ? "Success!" : "Unexpected error"}
        </Typography>
        <Link to="/">
          <Button>Return home</Button>
        </Link>
      </Box>
    );
  }

  return (
    <Container>
      <Box display="flex" flexDirection="column" my="53px">
        <Typography fontSize="24px" color="primary" fontWeight="600">
          {edit ? "Edit book" : "Add a new book"}
        </Typography>
        <Box display="flex" flexDirection="column" mt="57px" mb="38px">
          <TextField
            generalTouched={formTouched}
            ref={nameRef}
            required
            label="Name"
            defaultValue={book?.name}
          />
          <TextField
            generalTouched={formTouched}
            ref={authorRef}
            required
            mt="38px"
            label="Author"
            defaultValue={book?.author}
          />
          <TextField
            generalTouched={formTouched}
            ref={descriptionRef}
            required
            mt="38px"
            multiline
            label="Description"
            defaultValue={book?.description}
          />
          <TextField
            generalTouched={formTouched}
            ref={coverRef}
            required
            mt="38px"
            label="Cover URL"
            defaultValue={book?.cover}
          />
        </Box>
        <Box display="flex">
          {edit && (
            <Button mr={2} width="100%" background="black" onClick={onDelete}>
              Delete book
            </Button>
          )}
          <Button width="100%" onClick={onSubmit}>
            {edit ? "Edit book" : "Add new book"}
          </Button>
        </Box>
      </Box>
    </Container>
  );
};

export default BookForm;
