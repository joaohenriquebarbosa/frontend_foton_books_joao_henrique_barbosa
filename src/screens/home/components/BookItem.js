import React from "react";
import Typography from "../../../components/Typography";
import { Cover } from "../../../styles/global.style";
import { BooksItemWapper } from "../home.style";

const BookItem = ({ name, author, cover, id }) => {
  return (
    <BooksItemWapper to={`/book/${id}`}>
      <Cover width="105" height="153" alt="book cover" src={cover} />
      <Typography
        color="bookTitle"
        fontWeight="700"
        fontSize="12px"
        marginTop="9px"
        width="105px"
        elipsis
      >
        {name}
      </Typography>
      <Typography
        color="bookTitle"
        fontWeight="900"
        fontSize="10px"
        marginTop="5px"
        width="105px"
        elipsis
      >
        {author}
      </Typography>
    </BooksItemWapper>
  );
};

export default BookItem;
