import React from "react";
import Loader from "react-loader-spinner";
import Box from "../../../components/Box";
import theme from "../../../styles/theme";
import { BooksListWapper } from "../home.style";
import BookItem from "./BookItem";

const BooksList = ({ books, loading }) => {
  if (loading) {
    return (
      <Box display="flex" justifyContent="center" mt="36px" width="100%">
        <Loader
          type="TailSpin"
          color={theme.palette.primary}
          height={80}
          width={80}
        />
      </Box>
    );
  }

  return (
    <BooksListWapper>
      {(books || []).map((book) => (
        <BookItem
          key={book._id}
          id={book._id}
          name={book.name}
          author={book.author}
          cover={book.cover}
        />
      ))}
    </BooksListWapper>
  );
};

export default BooksList;
