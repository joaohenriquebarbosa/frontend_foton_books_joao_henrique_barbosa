import { Link } from "react-router-dom";
import styled from "styled-components";

export const HomeWrapper = styled.div`
  padding-top: 50px;
`;

export const BooksListWapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  margin-top: 36px;

  @media (min-width: 360px) {
    grid-template-columns: repeat(3, 1fr);
  }
  @media (min-width: 600px) {
    grid-template-columns: repeat(4, 1fr);
  }
  @media (min-width: 700px) {
    grid-template-columns: repeat(5, 1fr);
  }
  @media (min-width: 800px) {
    grid-template-columns: repeat(6, 1fr);
  }
  @media (min-width: 900px) {
    grid-template-columns: repeat(7, 1fr);
  }
  @media (min-width: 1000px) {
    grid-template-columns: repeat(8, 1fr);
  }
  @media (min-width: 1100px) {
    grid-template-columns: repeat(9, 1fr);
  }
  @media (min-width: 1200px) {
    grid-template-columns: repeat(10, 1fr);
  }
  @media (min-width: 1300px) {
    grid-template-columns: repeat(11, 1fr);
  }
`;

export const BooksItemWapper = styled(Link)`
  justify-self: center;
  margin-bottom: 12px;
  text-decoration: none;

  img {
    box-shadow: 0px 2px 4px rgb(208 208 208);
  }
`;
