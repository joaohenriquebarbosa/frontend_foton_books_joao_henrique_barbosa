import { debounce } from "lodash";
import React, { useEffect, useState } from "react";
import Box from "../../components/Box";
import TextField from "../../components/TextField";
import Typography from "../../components/Typography";
import booksService from "../../services/books.service";
import { Container } from "../../styles/layout.style";
import BooksList from "./components/BooksList";
import { HomeWrapper } from "./home.style";

const getBooks = debounce(
  (searchText, callback) =>
    booksService.getBooks(searchText).then((resp) => callback(resp)),
  300
);

const Home = () => {
  const [books, setBooks] = useState();
  const [searchText, setSearchText] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    getBooks(searchText, (resp) => {
      setBooks(resp);
      setLoading(false);
    });
  }, [searchText]);

  return (
    <HomeWrapper>
      <Container>
        <TextField
          search
          placeholder="Search book"
          value={searchText}
          onChange={(ev) => setSearchText(ev.target.value)}
        />
        {searchText === "" && (
          <Box mt="30px" display="flex">
            <Typography fontSize="24px" color="textAccent">
              Hi,
            </Typography>
            <Typography fontSize="24px" color="primary" fontWeight="600">
              &nbsp;Mehmed Al Fatih
            </Typography>
            <Box ml={1}>
              <img alt="hand" src="/assets/icons/hand.png" />
            </Box>
          </Box>
        )}
        <BooksList books={books} loading={loading} />
      </Container>
    </HomeWrapper>
  );
};

export default Home;
