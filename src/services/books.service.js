import { get, post, destroy, put } from "../config/api";

const booksService = {
  getBooks: (searchText) => {
    return get(`/books${searchText ? `?search=${searchText}` : ""}`);
  },
  getBook: (id) => {
    return get(`/books/${id}`);
  },
  deleteBook: (id) => {
    return destroy(`/books/${id}`);
  },
  createBook: (data) => {
    return post(`/books`, data);
  },
  updateBook: (id, data) => {
    return put(`/books/${id}`, data);
  },
};

export default booksService;
