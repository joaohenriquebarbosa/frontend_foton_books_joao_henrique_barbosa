import styled from "styled-components";

export const TextFieldWrapper = styled.div`
  /* width: 100%; */
  height: ${({ $multiline }) => ($multiline ? "228px" : "48px")};
  background: ${({ theme }) => theme.palette.background};
  box-shadow: ${({ $search }) =>
    $search
      ? "5px 5px 80px rgba(212, 173, 134, 0.122623)"
      : "5px 5px 80px rgba(212, 173, 134, 0.4926)"};
  border-radius: 10px;
  display: flex;
  align-items: center;
  padding: 0 16px 0 16px;

  img {
    height: 16px;
    margin-right: 16px;
    margin-top: 1px;
  }

  input::placeholder,
  ::-webkit-input-placeholder {
    font-weight: normal;
  }

  input {
    height: 18px;
  }

  textarea {
    height: 198px;
    resize: none;
  }

  input,
  textarea {
    width: 100%;
    background: none;
    border: none;
    outline: none;
    font-size: 16px;
    padding: 0;
    font-weight: ${({ $search }) => $search && "600"};
    font-family: "SF Pro Text", sans-serif !important;
    color: ${({ theme }) => theme.palette.searchText};
  }
`;
