import styled, { createGlobalStyle } from "styled-components";
import theme from "./theme";

export const Cover = styled.img`
  border-radius: 5px;
`;

export const ButtonWrapper = styled.button`
  box-shadow: 5px 5px 80px rgba(212, 173, 134, 0.4926);
  border: none;
  border-radius: 10px;
  height: 48px;
`;

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'SF Pro Display';
    src: url('/assets/fonts/FontsFree-Net-SFProDisplay-Regular.ttf');
  }

  @font-face {
    font-family: 'SF Pro Text';
    src: url('/assets/fonts/FontsFree-Net-SFProText-Regular.ttf');
  }

  @font-face {
    font-family: 'Roboto';
    src: url('/assets/fonts/Roboto-Regular.ttf');
  }

  * {
    font-family: 'SF Pro Display', sans-serif !important;
  }

  .active {
    filter: opacity(1) !important;
  }

  html, body, #root {
    background-color: #fefaf7;
    
  }

  .display {
    font-family: '${theme.fonts.display}', sans-serif !important;
  }

  .text {
    font-family: '${theme.fonts.text}', sans-serif !important;
  }
`;

export default GlobalStyle;
