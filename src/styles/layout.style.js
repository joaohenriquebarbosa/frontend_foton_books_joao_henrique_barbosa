import { NavLink } from "react-router-dom";
import styled from "styled-components";

export const Container = styled.div`
  margin-left: 20px;
  margin-right: 20px;
`;

export const TypographyWrapper = styled.div`
  ${({ elipsis }) =>
    elipsis
      ? `
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;`
      : ""}
`;

export const BottomNavigationWrapper = styled.div`
  background: #ffffff;
  height: 59px;
  display: flex;
  justify-content: center;
  position: fixed;
  bottom: 0;
  width: 100%;
  /* align-items: center; */
`;

export const BottomNavItem = styled(NavLink)`
  background: #ffffff;
  width: 125px;
  text-decoration: none;
  color: black;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  filter: opacity(0.3);
`;

export const AppWrapper = styled.div`
  padding-bottom: 59px;
`;
