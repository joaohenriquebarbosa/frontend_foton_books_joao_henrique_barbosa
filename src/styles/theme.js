const theme = {
  palette: {
    background: "#fdfcfc",
    searchText: "#000000",
    bookTitle: "rgba(49, 49, 49, 0.8)",
    text: "rgba(49, 49, 49, 0.6)",
    textAccent: "#36383A",
    bookActions: "#3F4043",
    primary: "#fe6a78",
  },
  fonts: {
    display: "SF Pro Display",
    text: "SF Pro Text",
  },
};

export default theme;
